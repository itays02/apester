// (function () {

    var imagesData = [];
    readFile();

    function readFile() {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", 'assets/data/gallery-data.json', false);
        rawFile.onreadystatechange = function () {
            if ((rawFile.readyState === 4) && ((rawFile.status === 200) || (rawFile.status == 0))) {
                imagesData = JSON.parse(rawFile.responseText);
            }
            else
                alert('There is a file error');
        }

        rawFile.send(null);
    }


    function getDataFileByIndex(startIndex, quantity)
    {
        var returnData = [];
        if((startIndex < imagesData.length) && (quantity > 0))
        {
            if(startIndex + quantity <= imagesData.length)
            {
                returnData = imagesData.slice(startIndex, startIndex + quantity);
            }
            else
                returnData = imagesData.slice(startIndex, imagesData.length);

            var id = 0;
            returnData.forEach(function (item) {
                item.imageId = id++;
                item.type = "color";
            });

        }

        return returnData;
    }

    function getImagesCount() {
        return imagesData.length;
    }