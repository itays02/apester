var underscore = angular.module('underscore', []);
underscore.factory('_', function() {
    return window._; //Underscore should be loaded on the page
});

angular.module('app.home', ['underscore'])
        .controller('HomeController', ['HomeService','$scope',
    function(HomeService,$scope) {

        var vm = this;
        var numOfImages = HomeService.getImagesCount();
        var selectedQuantity = 30;
        var rowBuffer = 2;
        var rowIndex = 0;

        vm.marginTop = 0;

        var imageHeight;
        var imageWidth = 0;
        var imagesInRow = 0;

        $scope.optimizePhoto = function(image,element) {

            var qualityIndex = imagesInRow * 4 ;
            var positionStart = imagesInRow *(rowBuffer) - 1;
            if(((image.imageId >= positionStart) && (image.imageId < qualityIndex + positionStart)) ||
            (rowIndex <= rowBuffer)){
                switch (image.type)
                {
                    case ('color'):
                    {
                        image.type = "thumbnail";
                        $scope.$apply(function () {
                            element[0].src = image.thumbnail.url;
                        });
                        break;
                    }

                    case ('thumbnail'):
                    {
                        image.type = "low";
                        $scope.$apply(function () {
                            element[0].src = image.low_resolution.url;
                        });
                        break;
                    }
                    case ('low'):
                    {
                        image.type = "standard";

                        $scope.$apply(function () {
                            element[0].src = image.standard_resolution.url;
                        });
                        break;
                    }

                }
            }

            else
            {
                // image.type = "thumbnail";
                // $scope.$apply(function () {
                //     element[0].src = image.thumbnail.url;
                // });
            }
        }

        var getFileData = function(startIndex) {

            if(startIndex < 0)
                startIndex = 0;
            return HomeService.getImagesData(startIndex, selectedQuantity);
        }

        // Set page height according to num of elements
        $scope.adjustScrollbar = function(){
            if(angular.element($('#image0'))[0]  != null) {
                imageHeight = angular.element($('#image0'))[0].offsetHeight;
                imageWidth = angular.element($('#image0'))[0].offsetWidth;
                imagesInRow = Math.floor(window.outerWidth / imageWidth);
                var rowCount =  Math.round(numOfImages / imagesInRow);

                $scope.$apply(function () {
                    vm.pageHeight = rowCount * imageHeight;
                });
            }
        }

        var updateImagesData = function(YPos, isFirst) {

            rowIndex = 0;
            if(imageHeight != null)
            {
                rowIndex = Math.floor(YPos / imageHeight);
            }

            var diff = rowIndex - rowBuffer;
            if(diff < 0) { diff = 0; }

            vm.marginTop = diff * imageHeight || 0 ;

            var imagesPart = getFileData(diff * imagesInRow);

            if(isFirst)
                vm.images = imagesPart;
            else
            {
                $scope.$apply(function () {
                    vm.images = imagesPart;
                });
            }
        }

        window.addEventListener("scroll", function(event) {

            if(scrollY <= vm.pageHeight) {
                updateImagesData(this.scrollY, false);
            }
        }, false);

        window.addEventListener('resize', function(event){

            $scope.adjustScrollbar();

        });

        // get first data
        updateImagesData(0, true);

    }
]);