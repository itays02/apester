    var app = angular.module('app.home');
var isInit = true;

    // Defining image feed item elements
    app.directive('imageonload', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                var callback = function() {
                    scope.optimizePhoto(scope.image,element);

                    if(isInit) {
                        scope.adjustScrollbar();
                        isInit = false;
                    }
                };

                element.bind('load', callback);
                element.bind('error', function () {
                    console.log("Error in loading image");
                });

            }
        };
    });