angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		// home page
		.when('/apesterapp/index.html', {
			templateUrl: 'app/components/home/homeView.html',
			controller: 'HomeController'
		});

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

}]);